package models;

import java.sql.Connection;

import wallDB.H2dbStarter;

public class ActiveRecord {

	private static Connection dbConnection = null;

	protected static Connection getDbConnection() {
		if (dbConnection == null) 
			dbConnection = H2dbStarter.getDbConnection();
		return dbConnection;
	}

	public static void setDbConnection(Connection dbConnection) {
		ActiveRecord.dbConnection = dbConnection;
	}

	
}
